//
//  CustomTabBarController.swift
//  fbMessenger
//
//  Created by mazodirty on 8/07/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit


class CustomTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = UICollectionViewFlowLayout()
        let friendsController = FriendsController(collectionViewLayout: layout)
        let recentMessagesNavigationController = UINavigationController(rootViewController: friendsController)
        recentMessagesNavigationController.tabBarItem.title = "Recent"
        recentMessagesNavigationController.tabBarItem.image = UIImage(named: "recent")
        
        let callsController = UIViewController()
        let callsNavigation = UINavigationController(rootViewController: callsController)
        callsNavigation.tabBarItem.title = "Calls"
        callsNavigation.tabBarItem.image = UIImage(named: "calls")
        
        let peopleNavigation = createDummyNavsController(title: "People", imageName: "people")
        let settingsNavigation = createDummyNavsController(title: "Settings", imageName: "settings")
        let groupsNavigation = createDummyNavsController(title: "Groups", imageName: "groups")
        
        viewControllers = [recentMessagesNavigationController, callsNavigation, groupsNavigation, peopleNavigation, settingsNavigation]
    }
    
    
    func createDummyNavsController(title: String, imageName: String) -> UINavigationController {
        let customViewController = UIViewController()
        let navigationController = UINavigationController(rootViewController: customViewController)
        navigationController.tabBarItem.title = title
        navigationController.tabBarItem.image = UIImage(named: imageName)
        
        return navigationController
    }
    
}














