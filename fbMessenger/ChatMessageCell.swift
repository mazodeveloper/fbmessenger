//
//  ChatMessageCell.swift
//  fbMessenger
//
//  Created by mazodirty on 8/07/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

class ChatMessageCell: UICollectionViewCell {
    
    var message: Message? {
        didSet {
            guard let message = message else {
                return
            }
            
            messageText.text = message.text
            let width = UIScreen.main.bounds.width / 3
            let messageWidth = width * 2
            let size = CGSize(width: messageWidth, height: 1000)
            let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 16)]
            let estimatedRect = NSString(string: message.text!).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
            
            messageTextWidthConstraint?.constant = estimatedRect.width + 16
            messageTextHeightConstraint?.constant = estimatedRect.height + 20
            
            bubbleHeightConstraint?.constant = estimatedRect.height + 20
            bubbleWidthConstraint?.constant = estimatedRect.width + 16 + 8
            
            //if the message isSender configure de constraint to the right otherwise configure the left constraint
            if message.isSender {
                bubbleLeftConstraint?.isActive = false
                bubbleRightConstraint?.isActive = true
                bubbleView.backgroundColor = UIColor(red: 0, green: 134/255, blue: 249/255, alpha: 1)
                messageText.textColor = UIColor.white
                profileImage.isHidden = true
            }else {
                bubbleLeftConstraint?.isActive = true
                bubbleRightConstraint?.isActive = false
                bubbleView.backgroundColor = UIColor(red: 230/255, green: 231/255, blue: 240/255, alpha: 1)
                messageText.textColor = UIColor.black
                profileImage.isHidden = false
            }
            
        }
    }
    
    let messageText: UITextView = {
        let message = UITextView()
        message.translatesAutoresizingMaskIntoConstraints = false
        message.font = UIFont.systemFont(ofSize: 16)
        message.isEditable = false
        message.backgroundColor = UIColor.clear
        
        return message
    }()
    
    let bubbleView: UIView = {
        let bubble = UIView()
        bubble.backgroundColor = UIColor(red: 230/255, green: 231/255, blue: 240/255, alpha: 1)
        bubble.translatesAutoresizingMaskIntoConstraints = false
        bubble.layer.cornerRadius = 15
        
        return bubble
    }()
    
    let profileImage: UIImageView = {
        let profile = UIImageView()
        profile.contentMode = .scaleAspectFit
        profile.clipsToBounds = true
        profile.layer.cornerRadius = 18
        profile.translatesAutoresizingMaskIntoConstraints = false
        
        return profile
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        
        setupViews()
    }
    
    var bubbleWidthConstraint: NSLayoutConstraint?
    var bubbleHeightConstraint: NSLayoutConstraint?
    var bubbleLeftConstraint: NSLayoutConstraint?
    var bubbleRightConstraint: NSLayoutConstraint?
    var messageTextWidthConstraint: NSLayoutConstraint?
    var messageTextHeightConstraint: NSLayoutConstraint?
    
    func setupViews() {
        addSubview(bubbleView)
        addSubview(messageText)
        addSubview(profileImage)
        
        profileImage.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2).isActive = true
        profileImage.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        profileImage.heightAnchor.constraint(equalToConstant: 36).isActive = true
        profileImage.widthAnchor.constraint(equalToConstant: 36).isActive = true
        
        bubbleView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        bubbleLeftConstraint = bubbleView.leftAnchor.constraint(equalTo: profileImage.rightAnchor, constant: 8)
        bubbleLeftConstraint?.isActive = true
        
        bubbleRightConstraint = bubbleView.rightAnchor.constraint(equalTo: rightAnchor, constant: -8)
        
        bubbleWidthConstraint = bubbleView.widthAnchor.constraint(equalToConstant: 100)
        bubbleWidthConstraint?.isActive = true
        bubbleHeightConstraint = bubbleView.heightAnchor.constraint(equalToConstant: 100)
        bubbleHeightConstraint?.isActive = true
        
        messageText.topAnchor.constraint(equalTo: topAnchor).isActive = true
        messageText.leftAnchor.constraint(equalTo: bubbleView.leftAnchor, constant: 8).isActive = true
        messageTextWidthConstraint = messageText.widthAnchor.constraint(equalToConstant: 100)
        messageTextWidthConstraint?.isActive = true
        messageTextHeightConstraint = messageText.heightAnchor.constraint(equalToConstant: 100)
        messageTextHeightConstraint?.isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}













