//
//  Message+CoreDataProperties.swift
//  fbMessenger
//
//  Created by mazodirty on 8/07/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import Foundation
import CoreData


extension Message {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Message> {
        return NSFetchRequest<Message>(entityName: "Message")
    }

    @NSManaged public var date: Date?
    @NSManaged public var text: String?
    @NSManaged public var isSender: Bool
    @NSManaged public var friend: Friend?

}
