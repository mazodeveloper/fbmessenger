//
//  FriendCell.swift
//  fbMessenger
//
//  Created by mazodirty on 7/07/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

class FriendCell: UICollectionViewCell {
    
    var message: Message? {
        didSet {
            guard let message = message else {
                return
            }
            
            profileImage.image = UIImage(named: message.friend!.profileImage!)
            profileImage.layer.cornerRadius = 40
            profileImageMini.image = UIImage(named: message.friend!.profileImage!)
            profileImageMini.layer.cornerRadius = 15
            
            nameUser.text = message.friend!.username!
            messageText.text = message.text
            
            if let date = message.date {
                
                let differenceInSeconds = Date().timeIntervalSince(date)
                let oneDayInSeconds: TimeInterval = 60 * 60 * 24
                if differenceInSeconds > oneDayInSeconds * 7 {
                    dateFormatter.dateFormat = "MM/dd/yy"
                }else if differenceInSeconds > oneDayInSeconds {
                    dateFormatter.dateFormat = "EEE"
                }
                else {
                    dateFormatter.dateFormat = "h:mm a"
                }
                
                lastConversation.text = dateFormatter.string(from: date)
            }
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? UIColor(red: 0, green: 134/255, blue: 249/255, alpha: 1) : UIColor.white
            nameUser.textColor = isHighlighted ? UIColor.white : UIColor.black
            messageText.textColor = isHighlighted ? UIColor.white : UIColor(white: 0.4, alpha: 1)
            lastConversation.textColor = isHighlighted ? UIColor.white : UIColor.black
        }
    }
    
    lazy var dateFormatter: DateFormatter = {
        
        let dateFormatter = DateFormatter()
        //"h:mm a" -> is the format to hours, minutes and a.m or p.m
        
        return dateFormatter
    }()
    
    let profileImage: UIImageView = {
        let profile = UIImageView()
        profile.clipsToBounds = true
        profile.contentMode = .scaleAspectFill
        profile.image = #imageLiteral(resourceName: "zuckprofile")
        profile.translatesAutoresizingMaskIntoConstraints = false
        
        return profile
    }()
    
    let profileImageMini: UIImageView = {
        let profile = UIImageView()
        profile.clipsToBounds = true
        profile.contentMode = .scaleAspectFill
        profile.image = #imageLiteral(resourceName: "zuckprofile")
        profile.translatesAutoresizingMaskIntoConstraints = false
        
        return profile
    }()
    
    let nameUser: UILabel = {
        let username = UILabel()
        username.numberOfLines = 1
        username.text = "Username"
        username.font = UIFont.boldSystemFont(ofSize: 16)
        username.translatesAutoresizingMaskIntoConstraints = false
        
        return username
    }()
    
    let messageText: UILabel = {
        let message = UILabel()
        message.numberOfLines = 1
        message.text = "This is a custom message"
        message.font = UIFont.systemFont(ofSize: 14)
        message.translatesAutoresizingMaskIntoConstraints = false
        message.textColor = UIColor(white: 0.4, alpha: 1)
        
        return message
    }()
    
    let lastConversation: UILabel = {
        let lastConversation = UILabel()
        lastConversation.numberOfLines = 1
        lastConversation.text = "12:30 pm"
        lastConversation.font = UIFont.systemFont(ofSize: 14)
        lastConversation.translatesAutoresizingMaskIntoConstraints = false
        
        return lastConversation
    }()
    
    let separatorLine: UIView = {
        let separator = UIView()
        separator.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 0.6)
        separator.translatesAutoresizingMaskIntoConstraints = false
        
        return separator
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        
        setupViews()
    }
    
    func setupViews() {
        addSubview(profileImage)
        addSubview(nameUser)
        addSubview(lastConversation)
        addSubview(messageText)
        addSubview(profileImageMini)
        addSubview(separatorLine)
        
        profileImage.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        profileImage.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        profileImage.widthAnchor.constraint(equalToConstant: 80).isActive = true
        profileImage.heightAnchor.constraint(equalToConstant:  80).isActive = true
        
        nameUser.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
        nameUser.leftAnchor.constraint(equalTo: profileImage.rightAnchor, constant: 8).isActive = true
        nameUser.rightAnchor.constraint(equalTo: lastConversation.leftAnchor, constant: -4).isActive = true
        
        lastConversation.topAnchor.constraint(equalTo: nameUser.topAnchor).isActive = true
        lastConversation.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        
        messageText.topAnchor.constraint(equalTo: nameUser.bottomAnchor, constant: 8).isActive = true
        messageText.leftAnchor.constraint(equalTo: nameUser.leftAnchor).isActive = true
        
        profileImageMini.topAnchor.constraint(equalTo: messageText.topAnchor).isActive = true
        profileImageMini.leftAnchor.constraint(equalTo: messageText.rightAnchor, constant: 8).isActive = true
        profileImageMini.widthAnchor.constraint(equalToConstant: 30).isActive = true
        profileImageMini.heightAnchor.constraint(equalToConstant:  30).isActive = true
        profileImageMini.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        
        separatorLine.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        separatorLine.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        separatorLine.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        separatorLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}























