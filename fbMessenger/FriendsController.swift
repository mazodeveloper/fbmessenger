//
//  ViewController.swift
//  fbMessenger
//
//  Created by mazodirty on 4/07/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit
import CoreData

class FriendsController: UICollectionViewController {
    
    struct IDS {
        static let friendCell = "FriendCell"
    }
    
    //var messages = [Message]()
    
    lazy var fetchedResultsController: NSFetchedResultsController<Friend> = {
        let fetchRequest: NSFetchRequest<Friend> = Friend.fetchRequest()
        let sortDate = NSSortDescriptor(key: "lastMessage.date", ascending: false)
        fetchRequest.sortDescriptors = [sortDate]
        let fetchedResults = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResults.delegate = self
        
        return fetchedResults
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*if let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }*/
        
        setupNavigation()
        //clearData()
        //Luego de crear los usuarios iniciales se debe comentar la linea.
        createTestMessages()
        //fetchRequestMessages()
        
        do {
            try fetchedResultsController.performFetch()
        }catch let error {
            print(error.localizedDescription)
        }
        
        collectionView?.backgroundColor = UIColor.white
        collectionView?.alwaysBounceVertical = true
        collectionView?.register(FriendCell.self, forCellWithReuseIdentifier: IDS.friendCell)
    }
    
    func clearData() {
        let entities = ["Message", "Friend"]
        do {
            for entity in entities {
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
                let objects = try context.fetch(fetchRequest) as? [NSManagedObject]
                for object in objects! {
                    context.delete(object)
                }
            }
            
            appDelegate.saveContext()
            
        }catch let error {
            print(error.localizedDescription)
        }
        
        
    }
    
    func createTestMessages() {
        let friend = Friend(context: context)
        friend.username = "Mark Zuckerberg"
        friend.profileImage = "zuckprofile"
        
        let message = Message(context: context)
        message.text = "Boy your app look so familiar"
        message.friend = friend
        message.date = Date().addingTimeInterval(-3 * 60)
        message.isSender = false
        
        let messageMark = Message(context: context)
        messageMark.text = "Boy your app look so familiar and I need tell you that facebook is the best social app in the world."
        messageMark.friend = friend
        messageMark.date = Date().addingTimeInterval(-2 * 60)
        messageMark.isSender = false
        
        let messageMark2 = Message(context: context)
        messageMark2.text = "Alright tihs app is awesome."
        messageMark2.friend = friend
        messageMark2.date = Date().addingTimeInterval(-1 * 60)
        messageMark2.isSender = false
        
        //response
        let messageMark3 = Message(context: context)
        messageMark3.text = "I had used react and is awesome with the virtual dom and of handle of states, is so easy to understand and use."
        messageMark3.friend = friend
        messageMark3.date = Date()
        messageMark3.isSender = true
        
        friend.lastMessage = messageMark3
        
        let friend2 = Friend(context: context)
        friend2.username = "Steve Jobs"
        friend2.profileImage = "steve_profile"
        
        let message2 = Message(context: context)
        message2.text = "I am the father and apple and iphone"
        message2.friend = friend2
        message2.date = Date().addingTimeInterval(-4 * 60)
        message2.isSender = false
        
        friend2.lastMessage = message2
        
        let friend3 = Friend(context: context)
        friend3.username = "Mahatma Gandhi"
        friend3.profileImage = "gandhi"
        
        let message3 = Message(context: context)
        message3.text = "Love, peace and joy."
        message3.friend = friend3
        message3.date = Date().addingTimeInterval(-12 * 60)
        message3.isSender = false
        
        friend3.lastMessage = message3
        
        let friend4 = Friend(context: context)
        friend4.username = "Donald trump"
        friend4.profileImage = "donald_trump_profile"
        
        let message4 = Message(context: context)
        message4.text = "I am Donald trump and hate you bro."
        message4.friend = friend4
        message4.date = Date().addingTimeInterval(-(60 * 24) * 60)
        message4.isSender = false
        
        friend4.lastMessage = message4
        
        let friend5 = Friend(context: context)
        friend5.username = "Hillary Clinton"
        friend5.profileImage = "hillary_profile"
        
        let message5 = Message(context: context)
        message5.text = "Vote for me I dont want to go the jail."
        message5.friend = friend5
        message5.date = Date().addingTimeInterval(-(60 * 24) * 60 * 7)
        message5.isSender = false
        
        friend5.lastMessage = message5
        
        appDelegate.saveContext()
        
    }
    
    /*func fetchRequestMessages() {
        if let friends = fetchFriends() {
            
            var messagesByFriends = [Message]()
            
            for friend in friends {
                let fetchRequest: NSFetchRequest<Message> = Message.fetchRequest()
                let sortDate = NSSortDescriptor(key: "date", ascending: false)
                fetchRequest.sortDescriptors = [sortDate]
                fetchRequest.predicate = NSPredicate(format: "friend.username = %@", friend.username!)
                fetchRequest.fetchLimit = 1
                
                do {
                    let fetchedMessages = try context.fetch(fetchRequest)
                    messagesByFriends.append(contentsOf: fetchedMessages)
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            
            self.messages = messagesByFriends.sorted(by: {$0.date!.compare($1.date!) == .orderedDescending})
        }
    }
    
    func fetchFriends() -> [Friend]? {
        let fetchRequest: NSFetchRequest<Friend> = Friend.fetchRequest()
        do {
            return try context.fetch(fetchRequest)
        }catch let error {
            print(error.localizedDescription)
        }
        
        return nil
    }*/
    
    func setupNavigation() {
        navigationController?.navigationBar.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.title = "Recents"
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionViewLayout.invalidateLayout()
    }
    
}

//MARK: -> UICollectionViewDelegate
extension FriendsController: UICollectionViewDelegateFlowLayout {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let count = fetchedResultsController.sections?[section].numberOfObjects {
            return count
        }
        
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IDS.friendCell, for: indexPath) as! FriendCell
        
        let friend = fetchedResultsController.object(at: indexPath)
        
        cell.message = friend.lastMessage
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width, height: 110)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let layout = UICollectionViewFlowLayout()
        let chatController = ChatController(collectionViewLayout: layout)
        let friend = fetchedResultsController.object(at: indexPath)
        chatController.friend = friend
        navigationController?.pushViewController(chatController, animated: true)
    }
    
}

//MARK: -> NSFetchedResultsControllerDelegate
extension FriendsController: NSFetchedResultsControllerDelegate {
    
    /*func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
    }*/
    
}







