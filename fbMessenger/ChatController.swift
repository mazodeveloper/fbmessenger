//
//  ChatController.swift
//  fbMessenger
//
//  Created by mazodirty on 7/07/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit
import CoreData

class ChatController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    struct IDS {
        static let chatCell = "ChatCell"
    }
    
    var friend: Friend? {
        didSet {
            guard let friend = friend else {
                return
            }
            
            messages = friend.messages?.allObjects as? [Message]
            messages = messages?.sorted(by: {$0.date!.compare($1.date!) == .orderedAscending})
        }
    }
    
    //CoreData por default no puede insertar en el collectionView mas de un Item, por lo cual debemos guardar cada insert en un array y luego de que se hallan guardado los nuevos items procedemos a insertarlos uno por uno en el metodo "controllerDidChangeContent"
    var blockOperations = [BlockOperation]()
    
    let containerMessage: UIView = {
        let container = UIView()
        container.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        container.translatesAutoresizingMaskIntoConstraints = false
        
        return container
    }()
    
    let messageTextField: UITextField = {
        let text = UITextField()
        text.placeholder = "Write a message..."
        text.translatesAutoresizingMaskIntoConstraints = false
        
        return text
    }()
    
    lazy var sendButton: UIButton = {
        let button = UIButton()
        button.setTitle("Send", for: .normal)
        let color = UIColor(red: 0, green: 134/255, blue: 249/255, alpha: 1)
        button.setTitleColor(color, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(sendNewMessage), for: .touchUpInside)
        
        return button
    }()
    
    var containerMessageBottomConstraint: NSLayoutConstraint?
    
    var messages: [Message]?
    
    lazy var fetchedResultsController: NSFetchedResultsController<Message> = {
        let fetchRequest: NSFetchRequest<Message> = Message.fetchRequest()
        let sortDate = NSSortDescriptor(key: "date", ascending: true)
        fetchRequest.sortDescriptors = [sortDate]
        fetchRequest.predicate = NSPredicate(format: "friend.username = %@", self.friend!.username!)
        
        let fetchedResults = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResults.delegate = self
        
        return fetchedResults
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = UIColor.white
        collectionView?.alwaysBounceVertical = true
        collectionView?.register(ChatMessageCell.self, forCellWithReuseIdentifier: IDS.chatCell)
        
        tabBarController?.tabBar.isHidden = true
        setupNavigationBar()
        setupMessageTextField()
        
        //notificationCenter
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        do {
            try fetchedResultsController.performFetch()
        }catch let error {
            print(error.localizedDescription)
        }
        
    }
    
    
    func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            
            if let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect {
                
                self.containerMessageBottomConstraint?.constant = -keyboardFrame.height
                
                UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                    self.view.layoutIfNeeded()

                }, completion: {(completed) in
                    let numberOfMessages = self.fetchedResultsController.sections![0].numberOfObjects
                    let lastIndexPath = IndexPath(item: numberOfMessages - 1, section: 0)
                    self.collectionView?.scrollToItem(at: lastIndexPath, at: .bottom, animated: true)
                })
            }
            
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let userInfo = notification.userInfo, let _ = userInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect {
            
            self.containerMessageBottomConstraint?.constant = 0
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                self.view.layoutIfNeeded()

            }, completion: nil)
            
        }
    }
    
    fileprivate func setupMessageTextField() {
        view.addSubview(containerMessage)
        containerMessage.addSubview(messageTextField)
        containerMessage.addSubview(sendButton)
        
        containerMessageBottomConstraint = containerMessage.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        containerMessageBottomConstraint?.isActive = true
        containerMessage.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        containerMessage.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        containerMessage.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        messageTextField.topAnchor.constraint(equalTo: containerMessage.topAnchor).isActive = true
        messageTextField.leftAnchor.constraint(equalTo: containerMessage.leftAnchor, constant: 8).isActive = true
        messageTextField.bottomAnchor.constraint(equalTo: containerMessage.bottomAnchor).isActive = true
        messageTextField.rightAnchor.constraint(equalTo: sendButton.leftAnchor).isActive = true
        
        sendButton.topAnchor.constraint(equalTo: messageTextField.topAnchor).isActive = true
        sendButton.rightAnchor.constraint(equalTo: containerMessage.rightAnchor).isActive = true
        sendButton.bottomAnchor.constraint(equalTo: containerMessage.bottomAnchor).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
    }
    
    func setupNavigationBar() {
        navigationItem.title = friend?.username?.capitalized
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Response", style: .plain, target: self, action: #selector(sendResponse))
    }
    
    func sendNewMessage() {
        if let text = messageTextField.text, text != "" {
            
            let message = Message(context: context)
            message.text = text
            message.date = Date()
            message.friend = friend!
            message.isSender = true
            
            appDelegate.saveContext()
            
            messageTextField.text = ""
            /*let indexPath = IndexPath(item: messages!.count, section: 0)
            messages?.append(message)
            collectionView?.insertItems(at: [indexPath])
            collectionView?.scrollToItem(at: indexPath, at: .bottom, animated: true)*/
        }
    }
    
    func sendResponse() {
        let message = Message(context: context)
        message.text = "This is a custom messge only for practice"
        message.date = Date()
        message.friend = friend!
        message.isSender = false
        
        let message2 = Message(context: context)
        message2.text = "Dude this is a custom message"
        message2.date = Date()
        message2.friend = friend!
        message2.isSender = false
        
        appDelegate.saveContext()
        
        /*let indexPath = IndexPath(item: messages!.count, section: 0)
        messages?.append(message)
        collectionView?.insertItems(at: [indexPath])
        collectionView?.scrollToItem(at: indexPath, at: .bottom, animated: true)*/
    }

}

//MARK: -> UICollectionViewDatasource and delegate
extension ChatController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let sections = fetchedResultsController.sections {
            return sections[section].numberOfObjects
        }
        
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IDS.chatCell, for: indexPath) as! ChatMessageCell
        if let image = friend?.profileImage {
            let message = fetchedResultsController.object(at: indexPath)
            cell.message = message
            cell.profileImage.image = UIImage(named: image)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let message = fetchedResultsController.object(at: indexPath)
        
        let width = UIScreen.main.bounds.width / 3
        let messageWidth = width * 2
        let size = CGSize(width: messageWidth, height: 1000)
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 16)]
        
        if let text = message.text {
            
            //Enviamos nuevamente el message para determinar el nuevo layout
            if let cell = collectionView.cellForItem(at: indexPath) as? ChatMessageCell {
                cell.message = message
            }
            
            let estimatedRect = NSString(string: text).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
            
            return CGSize(width: UIScreen.main.bounds.width, height: estimatedRect.height + 28)
        }
        
        return CGSize(width: UIScreen.main.bounds.width, height: 100)
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        messageTextField.resignFirstResponder()
    }

}


//MARK: -> NSFetchedResultsController
extension ChatController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                blockOperations.append(BlockOperation(block: {
                  self.collectionView?.insertItems(at: [indexPath])
                }))
            }
        case .delete:
            if let indexPath = indexPath {
                collectionView?.deleteItems(at: [indexPath])
            }
        case .update:
            if let indexPath = indexPath {
                let cell = collectionView?.cellForItem(at: indexPath) as! ChatMessageCell
                cell.message = fetchedResultsController.object(at: indexPath)
            }
        case .move:
            if let indexPath = indexPath, let newIndexPath = newIndexPath {
                collectionView?.insertItems(at: [newIndexPath])
                collectionView?.deleteItems(at: [indexPath])
            }
        }
        
    }
    
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView?.performBatchUpdates({
            for operation in self.blockOperations {
                operation.start()
            }
        }, completion: { (completed) in
            let lastItem = self.fetchedResultsController.sections![0].numberOfObjects - 1
            let indexPath = IndexPath(item: lastItem, section: 0)
            self.collectionView?.scrollToItem(at: indexPath, at: .bottom, animated: true)
        })
    }
    
}













